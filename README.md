# Laravel API Ecommerce Example


I will soon launch a video installation tutorial

## Installation

1. Clone the repo and `cd` into it
1. `composer install`
1. Rename or copy `.env.example` file to `.env`
1. `php artisan key:generate`
1. Set your database credentials in your `.env` file
1. Set your Mercadopago credentials into folter config `mercadopago.php`. with the information below:

## Config MP Credentials

```php
    return [
        'sand_box'  => true,
        'test'  => [
            'key'   => 'YOUR-TEST-KEY',
            'token'  => 'YOUR-TEST-TOKEN'
        ],
        'production'  => [
            'key'   => 'YOUR-PRODUCTION-KEY',
            'token' => 'YOUR-PRODUCT-TOKEN'
        ]
    ];
```

1. `php artisan migrate:fresh --seed`. This function was used to create the database and generate initial records
1. `php artisan serve --port=8080` or use Laravel Valet or Laravel Homestead
1. Using [postman](https://www.postman.com/downloads/) you can test the features of our api from the url `http://localhost:8080/api/categories`
1. Visit `http://localhost:8080/api/auth/login` using method `POST` if you want to obtain the token jwt to use on our frontend
